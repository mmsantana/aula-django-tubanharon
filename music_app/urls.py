from django.urls import path
from music_app.views import (
    AlbumListView, AlbumDetailView,
    CreateAlbumView, CreateMusicView,
    AlbumUpdateView, AlbumDeleteView
)

app_name = "music_app"

urlpatterns = [
    path('', AlbumListView.as_view()),
    path('album/<pk>', AlbumDetailView.as_view(), name="album_detail"),
    path('create_album/', CreateAlbumView.as_view(), name="create_album"),
    path('update_album/<pk>', AlbumUpdateView.as_view(), name="update_album"),
    path('delete_album/<pk>', AlbumDeleteView.as_view(), name="delete_album"),
    path('create_music/', CreateMusicView.as_view(), name="create_music"),
]
