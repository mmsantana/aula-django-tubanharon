from django.db import models


class ModificationsLog(models.Model):
    updated_at = models.DateTimeField("Atualizado em", auto_now_add=True)
    created_at = models.DateTimeField("Criado em", auto_now=True)

    class Meta:
        abstract = True


class Album(ModificationsLog):
    title = models.CharField("Título do Álbum", max_length=256)
    author = models.CharField("Nome do Autor/Banda", max_length=256)
    style = models.CharField("Estilo da Música", max_length=256)
    track_numbers = models.PositiveIntegerField("Número de Músicas")

    class Meta:
        verbose_name = 'Álbum'
        verbose_name_plural = 'Álbuns'

    def __str__(self):
        return f'{self.title} / {self.author}'


class Music(ModificationsLog):
    title = models.CharField("Título da Música", max_length=256)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    duration = models.CharField("Duração", max_length=256)

    class Meta:
        verbose_name = 'Música'
        verbose_name_plural = 'Músicas'

    def __str__(self):
        return f'{self.title} / {self.album}'
