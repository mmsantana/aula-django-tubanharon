from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .models import Album, Music
from .forms import AlbumForm, MusicForm


class AlbumListView(ListView):
    model = Album
    context_object_name = 'albums'
    template_name = 'albums_list.html'


class AlbumDetailView(DetailView):
    model = Album
    context_object_name = 'album'
    template_name = 'album_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['album_musics'] = Music.objects.filter(album=self.kwargs['pk'])
        context['album_data'] = [100, 10, 5, 2, 20, 30, 45]
        return context


class AlbumUpdateView(UpdateView):
    form_class = AlbumForm
    model = Album
    template_name = 'create_update_album.html'
    success_url = '/'


class AlbumDeleteView(DeleteView):
    model = Album
    context_object_name = 'album'
    success_url = '/'
    template_name = 'album_confirm_delete.html'


class CreateAlbumView(CreateView):
    form_class = AlbumForm
    template_name = 'create_update_album.html'
    success_url = '/'


class CreateMusicView(CreateView):
    form_class = MusicForm
    template_name = 'create_music.html'
    success_url = '/'
