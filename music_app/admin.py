from django.contrib import admin

from music_app.models import Album, Music


admin.site.register(Album)
admin.site.register(Music)
