# Generated by Django 2.2.6 on 2019-10-24 22:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated_at', models.DateTimeField(auto_now_add=True, verbose_name='Atualizado em')),
                ('created_at', models.DateTimeField(auto_now=True, verbose_name='Criado em')),
                ('title', models.CharField(max_length=256, verbose_name='Título do Álbum')),
                ('author', models.CharField(max_length=256, verbose_name='Nome do Autor/Banda')),
                ('style', models.CharField(max_length=256, verbose_name='Estilo da Música')),
                ('track_numbers', models.PositiveIntegerField(verbose_name='Número de Músicas')),
            ],
            options={
                'verbose_name': 'Álbum',
                'verbose_name_plural': 'Álbuns',
            },
        ),
        migrations.CreateModel(
            name='Music',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated_at', models.DateTimeField(auto_now_add=True, verbose_name='Atualizado em')),
                ('created_at', models.DateTimeField(auto_now=True, verbose_name='Criado em')),
                ('title', models.CharField(max_length=256, verbose_name='Título da Música')),
                ('duration', models.TimeField(verbose_name='Duração')),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='music_app.Album')),
            ],
            options={
                'verbose_name': 'Música',
                'verbose_name_plural': 'Músicas',
            },
        ),
    ]
