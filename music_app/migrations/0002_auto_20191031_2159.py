# Generated by Django 2.2.6 on 2019-10-31 21:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('music_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='music',
            name='duration',
            field=models.CharField(max_length=256, verbose_name='Duração'),
        ),
    ]
