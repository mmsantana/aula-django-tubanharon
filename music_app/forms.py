from django import forms
from django.forms import ModelForm

from .models import Album, Music


class AlbumForm(ModelForm):

    class Meta:
        model = Album
        fields = '__all__'

        widgets = {
            'title': forms.TextInput(attrs={
                'placeholder': 'Título do álbum', 'class': 'form-control',
            }),
            'author': forms.TextInput(attrs={
                'placeholder': 'Autor do álbum', 'class': 'form-control',
            }),
            'style': forms.TextInput(attrs={
                'placeholder': 'Estilo do álbum', 'class': 'form-control',
            }),
            'track_numbers': forms.TextInput(attrs={
                'placeholder': 'Número de músicas do álbum',
                'class': 'form-control',
            }),
        }


class MusicForm(ModelForm):

    class Meta:
        model = Music
        fields = '__all__'

        widgets = {
            'title': forms.TextInput(attrs={
                'placeholder': 'Título da música', 'class': 'form-control',
            }),
            'album': forms.Select(attrs={
                'class': 'form-control',
            }),
            'duration': forms.TextInput(attrs={
                'placeholder': 'Duração da música', 'class': 'form-control',
            }),
        }
